package com.rediff.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

public class Common {

	public Properties element = null;
	public Properties env = null;
	public WebDriver driver = null;
	public String projectPath = System.getProperty("user.dir");
	public Logger log = null;

	public void loadPropertyFiles() throws IOException {
		initializeLogger();
		InputStream file = new FileInputStream(projectPath + "/src/main/resoures/WebElements.properties");
		element = new Properties();
		element.load(file);
		log.debug("WebElements.properties file loaded");

		InputStream envFile = new FileInputStream(projectPath + "/src/main/resoures/EnvironmentDetails.properties");
		env = new Properties();
		env.load(envFile);
		log.debug("EnvironmentDetails.properties file loaded");
	}

	public void initializeLogger() {
		PropertyConfigurator.configure(projectPath + "/src/main/resoures/log4j.properties");

		log = Logger.getLogger("rootLogger");
		log.debug("Logger configured successfully");
	}

	public void closePropertiesFilesConnections() {
		element.clear();
	}

	public void openBrowsers() {
		String broserName=env.getProperty("browser");
		
		if(broserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", projectPath + "/src/main/resoures/drivers/chrome/chromedriver");
			driver = new ChromeDriver();
		}else if(broserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", projectPath + "/src/main/resoures/drivers/firefox/geckodriver");
			driver = new FirefoxDriver();
		}else if(broserName.equalsIgnoreCase("opera")) {
			System.setProperty("webdriver.opera.driver", projectPath + "/src/main/resoures/drivers/opera/operadriver");
			driver = new OperaDriver();
		}else if(broserName.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
		}else if(broserName.equalsIgnoreCase("edge")) {
			// firefox related logic....
		}else {
			throw new Error("Hey Please check the browser name in EnvironmentDetails.properties file");
		}
				
		driver.get(env.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		log.debug("in open browser method");

	}

	public void closeBrowsers() {
		driver.close();
		log.debug("in close browser method");
	}

	public Object[][] readExcel() throws EncryptedDocumentException, IOException {
		InputStream fis = new FileInputStream(projectPath + "/src/main/resoures/TestData.xlsx");
		Workbook workbook = WorkbookFactory.create(fis);
		Sheet sheet = workbook.getSheetAt(0);

		int totalRows = sheet.getLastRowNum() + 1;

		Row row = sheet.getRow(0);
		int totalCells = row.getLastCellNum();

		Object o[][] = new Object[totalRows][totalCells];

		// All Rows
		for (int i = 0; i < totalRows; i++) {
			// All cells in a row
			row = sheet.getRow(i);
			totalCells = row.getLastCellNum();
			for (int j = 0; j < totalCells; j++) {
				Cell cell = row.getCell(j);
				o[i][j] = cell.getStringCellValue();
				// log.debug(cell.getStringCellValue());
			}
		}
		return o;
	}

	public void captureScreenshots() throws IOException {
		TakesScreenshot screen = (TakesScreenshot) driver;
		File file = screen.getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		String screenshotName=date.toString().replace(" ", "_").replace(":", "_");
		FileUtils.copyFile(file, new File(projectPath + "/Screenshots/" + screenshotName + ".png"));
	}

	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		Common c = new Common();
		// log.debug(c.projectPath);
		// c.readExcel();

	}

}
